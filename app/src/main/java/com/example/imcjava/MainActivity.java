package com.example.imcjava;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout background;
    EditText kg;
    EditText size;
    Button calculate;
    TextView result;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        background = (RelativeLayout)this.findViewById(R.id.base);

        kg = (EditText)this.findViewById(R.id.etKg);
        kg.setClipToOutline(true);

        size = (EditText)this.findViewById(R.id.etSize);
        size.setClipToOutline(true);

        calculate = (Button)this.findViewById(R.id.calculate);
        calculate.setOnClickListener(this);

        result = (TextView)this.findViewById(R.id.result);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onClick(View v){
        if(!kg.getText().toString().equals("") && !size.getText().toString().equals("")){
            double kgResult = Double.parseDouble(kg.getText().toString().replace(",", "."));
            double sizeResult = Double.parseDouble(size.getText().toString().replace(",", "."));
            double resultImc = kgResult / (sizeResult * sizeResult);

            result.setText("Valor: " + String.format("%.1f", resultImc) + "");

            setBackground(resultImc);
        } else{
            Toast.makeText(MainActivity.this, "Informe valores válidos para peso e altura.", Toast.LENGTH_SHORT).show();
        }

    }

    public void setBackground(double value) {
        if(value < 16.0) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc1));
        } else if(value >= 16.0 && value <= 16.9) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc2));
        } else if(value >= 17.0 && value <= 18.4) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc3));
        } else if(value >= 18.5 && value <= 24.9) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc4));
        } else if(value >= 25.0 && value <= 29.9) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc5));
        } else if(value >= 30.0 && value <= 34.9) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc6));
        } else if(value >= 35.0 && value <= 39.9) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc7));
        } else if(value >= 40) {
            background.setBackgroundColor(ContextCompat.getColor(this, R.color.imc8));
        }
    }
}